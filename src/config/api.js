const axios = require('axios');

// Cambiar la baseURL segun la configuracion del servidor en el virtual host de apache
const API = axios.create({
    baseURL: `http://epayco.local/index.php/api/v1`
});

module.exports = API;
