const express = require('express');
const router = express.Router();

const payController = require('../controllers/payController');

module.exports = function() {
    // Rutas de la aplicacion
    router.post('/login_check', payController.LoginUser);
    router.post('/users/register', payController.RegisterUser);
    router.post('/wallet/create', payController.CreateWallet);
    router.post('/wallet/balance', payController.BalanceWallet);
    router.post('/wallet/increase', payController.IncreaseWallet);
    router.post('/payment/pay', payController.Pay);
    router.post('/payment/confirmation', payController.PayConfirmation);
    return router;
};
