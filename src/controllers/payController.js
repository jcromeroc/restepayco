const API = require('../config/api');

const formUrlEncoded = x =>
   Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, '');

exports.LoginUser = async (req, res) => {
    const { _email, _password } = req.body;

    await API({
        method: 'post',
        url: '/login_check',
        data: formUrlEncoded({
            _email,
            _password
        })
    })
    .then(function (response) {
        const { token } = response.data;
        res.json({
            code: 200,
            token
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "Login incorrecto",
            error: error.response.data.message
        });  
    });
};

exports.RegisterUser = async (req, res) => {
    const { name, email, password, document, phone } = req.body;
    
    await API({
        method: 'post',
        url: '/users/register',
        data: {
            name,
            email,
            password,
            document,
            phone
        }
    })
    .then(function (response) {
        const { token } = response.data;
        res.json({
            code: 200,
            token
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "El usuario no ha sido registrado",
            error: error.response.data.message
        });  
    });
};

exports.CreateWallet = async (req, res) => {
    const { token, email } = req.body;

    API.defaults.headers.common = {'Authorization': `Bearer ${token}`}
    
    await API({
        method: 'post',
        url: '/wallet/create',
        data: {
            email
        }
    })
    .then(function (response) {
        const { data } = response.data;
        res.json({
            code: 200,
            data
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "La cartera no ha sido creada",
            error: error.response.data.message
        });
    // console.error("Error response:");
    // console.error(error.response.data);    // ***
    // console.error(error.response.status);  // ***
    // console.error(error.response.headers); // ***  
    });
};

exports.BalanceWallet = async (req, res) => {
    const { token, email, document, phone } = req.body;

    API.defaults.headers.common = {'Authorization': `Bearer ${token}`}
    
    await API({
        method: 'get',
        url: '/wallet/balance',
        data: {
            email,
            document,
            phone
        }
    })
    .then(function (response) {
        const { data, balance } = response.data;
        res.json({
            code: 200,
            data,
            balance
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "No se puede consultar el saldo de la cartera",
            error: error.response.data.message
        });  
    });
};

exports.IncreaseWallet = async (req, res) => {
    const { token, email, document, phone, amount } = req.body;

    API.defaults.headers.common = {'Authorization': `Bearer ${token}`}
    
    await API({
        method: 'post',
        url: '/wallet/increase',
        data: {
            email,
            document,
            phone,
            amount
        }
    })
    .then(function (response) {
        const { data, balance } = response.data;
        res.json({
            code: 200,
            data,
            balance
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "No se ha podido recargar la cartera",
            error: error.response.data.message
        });  
    });
};

exports.Pay = async (req, res) => {
    const { token, email, amount } = req.body;

    API.defaults.headers.common = {'Authorization': `Bearer ${token}`}
    
    await API({
        method: 'post',
        url: '/payment/pay',
        data: {
            email,
            amount
        }
    })
    .then(function (response) {
        const { data, codigoPago } = response.data;
        res.json({
            code: 200,
            data,
            codigoPago
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "No se ha podido realizar el pago",
            error: error.response.data.message
        });  
    });
};

exports.PayConfirmation = async (req, res) => {
    const { token, email, codigoPago, tokenPay } = req.body;

    API.defaults.headers.common = {'Authorization': `Bearer ${token}`}
    
    await API({
        method: 'post',
        url: '/payment/confirmation',
        data: {
            email,
            codigoPago,
            token: tokenPay
        }
    })
    .then(function (response) {
        const { data, balance } = response.data;
        res.json({
            code: 200,
            data,
            balance
        });  
    })
    .catch(function (error) {
        res.json({
            code: 400,
            data: "No se ha podido confirmar el pago",
            error: error.response.data.message
        });  
    });
};