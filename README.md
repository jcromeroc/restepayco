Epayco
===============



Instalación del API
===============

1. Bajar el proyecto y en directorio raíz ejecutar el comando: npm install.
2. Si es necesario cambiar el puerto, modificar el archivo src/index.js en la línea 5; establecer el puerto que esté libre en su computadora. Recordar que si se hace este cambio aquí, también debe hacerlo en el frontend.
3. Definir la ruta del backend con symfony en el archivo src\config\api.js en la línea 5.



Uso del API
===============

Ejecutar en la raíz del proyecto el comando: npm start.

Al hacer esto el servidor estará escuchando en el puerto designado.

Listo para usar el API!!!
